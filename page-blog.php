<?php
/*
* Template Name: Blog
*/
get_header(); 

$args = array(
  'post_type' => 'post',
  'post_status' => 'publish',
  'posts_per_page' => 10,  
);
?>

<main>

    <div class="container">
        <h1>Blog</h1>
      <div class="row">
      <?php $the_query = new WP_Query( $args ); ?>
      <?php if ( $the_query->have_posts() ) : ?>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>      
            <div class="col-md-4 item">
              <div class="card">
                
                <?php if(get_the_post_thumbnail()): ?>
                <?= get_the_post_thumbnail(get_the_ID(), 'thumbnail', $attr); ?>
                <?php else: ?>
                 <?='<img src="'. get_template_directory_uri(). '/assets/images/nature.jpg' .'" />'; 
                 endif;
                ?>

                <a href="<?= the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
                <p><?php the_excerpt(); ?></p>

                <a href="<?= the_permalink(); ?>" class="btn btn-leia">Leia mais</a>
              </div>
            </div>
        <?php endwhile; ?>

      <?php wp_reset_postdata(); ?>
      <?php else : ?>
        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
      <?php endif; ?>

      </div>

    </div>


</main>


<?php get_footer(); ?>