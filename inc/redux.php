<?php
    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    $opt_name = 'opcoes';

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        'display_name'         => $theme->get( 'Name' ),       
        'menu_title'           => esc_html__( 'Opções do Tema', 'blank-options' ),
        'customizer'           => true,
    );

    Redux::setArgs( $opt_name, $args );

    // header options
   Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Header', 'blank-options' ),
        'id'     => 'header',
        'icon'   => 'el el-home',
    ));

     Redux::set_fields( $opt_name, 'header',  array(
       array (
        'id'       => 'logo',
        'type'     => 'media',
        'title'    => esc_html__( 'Logo', 'blank-options' ),
        'subtitle' => esc_html__( 'Selecione o logo do site', 'blank-options' ),
        'default'  => array(
            'url' => get_template_directory_uri() . '/assets/images/logo.png',
        ),
       ),
        array (
          'id'       => 'bg_header',
          'type'     => 'color_rgba',
          'title'    => esc_html__( 'Cor de Fundo', 'blank-options' ),
          'subtitle' => esc_html__( 'Selecione a cor de fundo do header', 'blank-options' ),
        ),
        array (
          'id'       => 'font-color',
          'type'     => 'color_rgba',
          'title'    => esc_html__( 'Cor da Fonte', 'blank-options' ),
          'subtitle' => esc_html__( 'Selecione a cor da fonte', 'blank-options' ),
        ),
     ));

     // header fim


     // footer
      Redux::setSection( $opt_name, array( 
        'title'  => esc_html__( 'Footer', 'blank-options' ),
        'id'     => 'footer',
        'icon'   => 'el el-home',
      ));
    
      Redux::set_fields( $opt_name, 'footer',  array(
        array (
          'id'       => 'logo-footer',
          'type'     => 'media',
          'title'    => esc_html__( 'Logo', 'blank-options' ),
          'subtitle' => esc_html__( 'Selecione o logo do site', 'blank-options' ),
          'default'  => array(
              'url' => get_template_directory_uri() . '/assets/images/logo.svg',
          ),
         ),
        array (
          'id'       => 'bg_footer',
          'type'     => 'color_rgba',
          'title'    => esc_html__( 'Cor de Fundo', 'blank-options' ),
          'subtitle' => esc_html__( 'Selecione a cor de fundo do footer', 'blank-options' ),
        ),
      ));