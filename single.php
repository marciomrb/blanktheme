<?php
get_header();
?>
<article>

 <div class="headArticle">
        <div class="container">
          <h1><?php the_title(); ?></h1>
        </div>

        <div class="overlay"></div>
          <?php if(get_the_post_thumbnail()): ?>
            <?= get_the_post_thumbnail(get_the_ID(), 'large', $attr); ?>
          <?php else: ?>
            <?='<img src="'. get_template_directory_uri(). '/assets/images/nature.jpg' .'" />'; ?>
          <?php endif; ?>
      </div> 
  </div>


  <div class="container content">    
    <?php the_content(); ?>
  </div>
</article>



<?php get_footer(); ?>