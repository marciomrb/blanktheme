<?php 
wp_footer(); 
global $opcoes;
$bg_footer = $opcoes['bg_footer']['rgba'];
?>


<footer style="background-color: <?= $bg_footer; ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-12"><img src="<?= $opcoes['logo-footer']['url']; ?>" /></div>
            <div class="col-md-12">
                <p>&copy; <?=  bloginfo('name') . " " . date("Y") ?></p>
            </div>
        </div>          
    </div>
</footer>

   
  </body>
</html>
