<?php
  global $opcoes;
  $logo = $opcoes['logo']['url'];
  $bg_header = $opcoes['bg_header']['rgba'];
  $fontColor = $opcoes['font-color']['rgba']; 
?>

<!DOCTYPE html>
<html lang="pt-BR">
  <head>      
    <?php wp_head(); ?> 
    <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
    <style>
      nav .navbar-nav ul li a {
        color: <?= $fontColor; ?>;
      }
    </style>
  </head>
  <body <?= body_class(); ?>>
    <nav class="navbar navbar-expand-lg sticky-top" style="background-color: <?= $bg_header; ?>">
      <div class="container">
        <?php
        if ($logo):
           echo '<a class="navbar-brand" href="' . home_url() .'"><img src="' . $logo . '" alt="' . get_bloginfo( 'name' ) . '"></a>';
        else:
            echo '<a class="navbar-brand" href="'. home_url() .'"><img src="'. get_template_directory_uri() . '/assets/images/logo.svg' .'"></a>';
        endif;
        ?>
        
       

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>


        <div class="collapse navbar-collapse navbar-custom d-lg-flex justify-content-lg-end" id="navbarSupportedContent">
            <?php wp_nav_menu( array( 'theme_location' => 'Header', 'menu_class' => 'navbar-nav ms-auto mb-2 mb-lg-0' ) ); ?>        
        </div>
      </div>
    </nav>