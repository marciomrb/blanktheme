<?php
/*
* Template Name: Home
*/
get_header(); 
?>

<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12 item">
        <h1 data-aos="fade-down" data-aos-delay="300" data-aos-duration="500">Bem-vindo ao <?php bloginfo('name'); ?></h1>  
        <p data-aos="fade-up" data-aos-delay="600" data-aos-duration="2000"><?php bloginfo('description'); ?></p>  
        <button onclick="testSwal()">Clique aqui para ver o Sweet Alert</button>
      </div>
    </div>
  </div>
  </div>
</section>


<?php get_footer(); ?>