function totop() {
  window.scrollTo({
    top: 0,
    behavior: 'smooth'
  })
}

AOS.init({
  disable: 'mobile',
});

function testSwal() {
  Swal.fire({
    title: 'Bem Vindos',
    icon: 'info',
    html:
      'ao ' +
      '<strong>Blank Theme!</strong>',  
    confirmButtonText: 'Ok!',
  })
}
// dropdown do bootstrap funcionar
$(document).ready(function(){
  $('ul#menu-header li.dropdown a').attr('role', 'button').attr('data-bs-toggle', 'dropdown').attr('aria-expanded', 'false').addClass('dropdown-toggle');
  $('ul.sub-menu li a').removeAttr('role').removeAttr('data-bs-toggle').removeAttr('aria-expanded').removeClass('dropdown-toggle');
  $('a[href$=".pdf"]').attr('download', '').attr('target', '_blank');
});
