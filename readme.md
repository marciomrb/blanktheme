** 
## Blank Theme Wordpress


> Criado para facilitar o desenvolvimento de temas para wordpress.



**

## CSS - SASS
Diretorio: (assets/css/main.scss)


 **-- Colors**

     - $primary: #00bcd4; 
     - $secondary: #ff9800; 
     - $success: #4caf50; 
     - $info: #2196f3; 
     - $warning: #ffc107; 
     - $danger: #f44336; 
     - $light: #f1f1f1; 
     - $dark: #212121; 
     - $white: #ffffff;
     - $black: #000000; 
     - $gray: #9e9e9e; 
     - $gray-light: #f5f5f5; 
     - $gray-dark: #616161; 
     - $gray-darker: #424242;
     - $gray-darkest: #212121;
     - $gray-lightest: #f5f5f5;

  

 **-- Fonts Weight**

     - $fw-thin: 100; 
     - $fw-extra-light: 200;
     - $fw-light: 300;
     - $fw-normal: 400;
     - $fw-medium: 500;
     - $fw-semi-bold: 600;
     - $fw-bold: 700;
     - $fw-extra-bold: 800;
     - $fw-black: 900;


 **-- Media Queries**

     - $xs: 0;
     - $sm: 576px;
     - $md: 768px;
     - $lg: 992px;
     - $xl: 1200px;
     - $xxl: 1400px; 

**

## Functions
Diretorio: (/functions.php)

 -  Algumas funções foram adicionadas, para trocar a class do bootstrap `(linha 108 e 114)`. 
 -  remover auto p e span do contact form 7 `(linha 8)`.
 - para voltar aparecer a barra de admin, remover a action na `(linha 7)`
 - na `linha 126` existe uma function pra pegar post types (comentada), apenas como exemplo
 - na `linha 140` começa o array com os plugins que serão instalados automaticamente, a lista de plugins fica logo abaixo, seguindo de nome, url e diretorio da pasta onde se encontra o instalador.

**-- Styles Enqueue**

    - Bootstrap CSS 5    
    - Swiper Slider    
    - Aos (Animação)    
    - Main CSS (theme css)
    - Animate CSS

**

  

**-- Scripts Enqueue**  

    - Bundle
    - jQuery 3.6
    - SweetAlert
    - Swiper Slider
    - Aos (animação)
    - Main (theme script)

> Estão com variáveis, para ser possível a atualização do bootstrap, swiper, etc...

**  

**-- Plugins que irão se auto-instalar:**

    - Advanced Custom
    - Fields Custom Post Type UI (CPT UI)
    - Editor Clássico
    - SVG Support

  **