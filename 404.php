<?php
get_header(); 
?>

<section class="error404">
  <div class="container">
    <div class="row">
      <div class="col-md-12 item">
        <h1>404</h1>
        <h2>Página não encontrada</h2>
        <p>Desculpe, a página que você está procurando não existe ou não está mais disponível.</p>
        <a href="<?= home_url(); ?>" class="btn btn-voltar">Voltar para o site</a>
      </div>      
    </div>
  </div>
</section>

<?php get_footer(); ?>