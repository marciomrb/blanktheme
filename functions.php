<?php
/**
 * Blank functions and definitions
 *
 */

add_filter('show_admin_bar', '__return_false'); // remove barra do admin
add_filter('wpcf7_autop_or_not', '__return_false'); // remove auto p do contact form 7

// remover span do contact form
add_filter('wpcf7_form_elements', function($content) {
	$content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
	return $content;
});

// redux
require_once(get_template_directory() . '/inc/redux.php');

// meta tags
function MetaTags() {
	echo '<meta charset="UTF-8" />';	
	echo '<meta http-equiv="X-UA-Compatible" content="IE=edge" />';
	echo '<meta name="viewport" content="width=device-width, initial-scale=1.0" />';
}
add_action('wp_head', 'MetaTags');

 // Styles Blank

function BlankStyles() {
	$BoostrapCSS = 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css';
	$SwiperCSS = 'https://unpkg.com/swiper@8/swiper-bundle.min.css';
	$aosCSS = 'https://unpkg.com/aos@2.3.1/dist/aos.css';
	$AnimateCSS = 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css';

	wp_register_style( 'Bootstrap', $BoostrapCSS, false, '1' );
	wp_enqueue_style( 'Bootstrap' );

	wp_register_style( 'Swiper', $SwiperCSS, false, '1' );
	wp_enqueue_style( 'Swiper' );

	wp_register_style( 'AOS', $aosCSS, false, '1' );
	wp_enqueue_style( 'AOS' );

	wp_register_style( 'Animate', $AnimateCSS, false, '1' );
	wp_enqueue_style( 'Animate' );

	wp_register_style( 'Main', get_template_directory_uri() . '/assets/css/main.css', array(), rand(111,9999), 'all' );
	wp_enqueue_style( 'Main' );
}
add_action( 'wp_enqueue_scripts', 'BlankStyles' );


// Scripts Blank
function UniScripts() {

	$jQuery = 'https://code.jquery.com/jquery-3.6.0.js';
	$AosJS = 'https://unpkg.com/aos@2.3.1/dist/aos.js';
	$SweetAlert = 'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.0/sweetalert2.all.min.js';
	$SwiperJS = 'https://unpkg.com/swiper@8/swiper-bundle.min.js';
	$BootstrapJS = 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js';

	wp_deregister_script( 'jQuery' ); // remove o jquery antigo
	wp_register_script( 'jQuery', $jQuery, false, '1', false );
	wp_enqueue_script( 'jQuery' );

	wp_register_script( 'AOS', $AosJS, false, '1', true );
	wp_enqueue_script( 'AOS' );

	wp_register_script( 'SweetAlert', $SweetAlert, false, '1', true );
	wp_enqueue_script( 'SweetAlert' );

	wp_register_script( 'Swiper', $SwiperJS, false, '1', true );
	wp_enqueue_script( 'Swiper' );

	wp_register_script( 'Bootstrap', $BootstrapJS, false, '1', true );
	wp_enqueue_script( 'Bootstrap' );

  wp_register_script( 'Main',  get_template_directory_uri() . '/assets/js/main.js', array(), rand(111,9999), true );
	wp_enqueue_script( 'Main' );

}
add_action( 'wp_enqueue_scripts', 'UniScripts' );


// Defer nos scripts
function mind_defer_scripts( $tag, $handle, $src ) {
  $defer = array( 
		'Bootstrap',
  );
  if ( in_array( $handle, $defer ) ) {
     return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>' . "\n";
  }    
    return $tag;
} 
add_filter( 'script_loader_tag', 'mind_defer_scripts', 10, 3 );







function BlankMenu() {
	$locations = array(
		'Header' => __( 'Menu Principal Header', 'blank' ),
		'Social Media' => __( 'Menu Social do Footer', 'blank' ),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'BlankMenu' );


// mudar class dos links do menu
function add_menu_link_class($atts, $item, $args) {
	$atts['class'] = 'nav-link';
	return $atts;
}
add_filter('nav_menu_link_attributes', 'add_menu_link_class', 1, 3);

function my_nav_menu_submenu_css_class( $classes ) {
$classes[] = 'dropdown-menu';
return $classes;
}
add_filter( 'nav_menu_submenu_css_class', 'my_nav_menu_submenu_css_class' );
// mudar class dos links do menu





// Get Post Type
function getPosts() {
	$args = array(  
			'post_type' => 'posts',
			'post_status' => 'publish',
      'orderby'   => 'meta_value',
      'order' => 'ASC',
			'posts_per_page' => 9
	);
	$loop = new WP_Query( $args );
	return $loop->posts;
}


// plugins instalados automaticamente
// $plugins = array(
// 	array('name' => 'Advanced Custom Fields', 'path' => 'https://downloads.wordpress.org/plugin/advanced-custom-fields.5.11.4.zip', 'install' => 'advanced-custom-fields/acf.php'),
// 	array('name' => 'Custom Post Type UI', 'path' => 'https://downloads.wordpress.org/plugin/custom-post-type-ui.1.10.2.zip', 'install' => 'custom-post-type-ui/custom-post-type-ui.php'),
// 	array('name' => 'Editor Classico', 'path' => 'https://downloads.wordpress.org/plugin/classic-editor.1.6.2.zip', 'install' => 'classic-editor/classic-editor.php'),
// 	array('name' => 'SVG Support', 'path' => 'https://downloads.wordpress.org/plugin/svg-support.2.4.2.zip', 'install' => 'svg-support/svg-support.php'),
// );
// mm_get_plugins($plugins);

// function mm_get_plugins($plugins) {
//   $args = array(
//     'path' => ABSPATH.'wp-content/plugins/',
//     'preserve_zip' => false
//   );

//     foreach($plugins as $plugin){
//       mm_plugin_download($plugin['path'], $args['path'].$plugin['name'].'.zip');
//       mm_plugin_unpack($args, $args['path'].$plugin['name'].'.zip');
//       mm_plugin_activate($plugin['install']);
//     }
// }
// function mm_plugin_download($url, $path) {
//     $ch = curl_init($url);
//     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//     $data = curl_exec($ch);
//     curl_close($ch);
//     if(file_put_contents($path, $data))
//       return true;
//     else
//     return false;
// }
// function mm_plugin_unpack($args, $target) {
//     if($zip = zip_open($target)) {
//       while($entry = zip_read($zip)){
//           $is_file = substr(zip_entry_name($entry), -1) == '/' ? false : true;
//           $file_path = $args['path'].zip_entry_name($entry);
//             if($is_file) {
//               if(zip_entry_open($zip,$entry,"r")) {
//                 $fstream = zip_entry_read($entry, zip_entry_filesize($entry));
//                 file_put_contents($file_path, $fstream );
//                 chmod($file_path, 0777);
//                 //echo "save: ".$file_path."<br />";
//               }
//               zip_entry_close($entry);
//               } else {
// 									if(zip_entry_name($entry)) {
// 										mkdir($file_path);
// 										chmod($file_path, 0777);
// 										//echo "create: ".$file_path."<br />";
// 									}
//                 }
//             }
//             zip_close($zip);
//     }
//     if($args['preserve_zip'] === false) {
//             unlink($target);
//     }
// }
// function mm_plugin_activate($installer) {
//     $current = get_option('active_plugins');
//     $plugin = plugin_basename(trim($installer));
//     if(!in_array($plugin, $current)) {
//       $current[] = $plugin;
//       sort($current);
//       do_action('activate_plugin', trim($plugin));
//       update_option('active_plugins', $current);
//       do_action('activate_'.trim($plugin));
//       do_action('activated_plugin', trim($plugin));
//         return true;
//     } else {
// 			return false;
// 		}
           
// }

// button to Top;
function ButtonToTop() {
  echo 
  '<div id="totop" onclick="totop();" class="animate__animated animate__bounce animate__infinite animate__slower" >
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M352 352c-8.188 0-16.38-3.125-22.62-9.375L192 205.3l-137.4 137.4c-12.5 12.5-32.75 12.5-45.25 0s-12.5-32.75 0-45.25l160-160c12.5-12.5 32.75-12.5 45.25 0l160 160c12.5 12.5 12.5 32.75 0 45.25C368.4 348.9 360.2 352 352 352z"/></svg>
  </div>';  
}
add_action('wp_footer', 'ButtonToTop');


// theme support
add_theme_support( 'post-thumbnails');

function CustomLogo() {
  $defaults = array(
      'height'               => 600,
      'width'                => 600,
      'flex-height'          => true,
      'flex-width'           => true,
      'header-text'          => array( 'site-title', 'site-description' ),
      'unlink-homepage-logo' => true, 
  );

  add_theme_support( 'custom-logo', $defaults );
}

add_action( 'after_setup_theme', 'CustomLogo' );